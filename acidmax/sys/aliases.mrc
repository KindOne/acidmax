loadnex load.scripts
join {
  if ($1 == $null) { $ae $space(2) 4[Error]:14 not enough parameters  | serror | halt }
  elseif ($server == $null) { $ae 0,4ERROR|0,14 Not connected to server.  | serror | halt }
  if ($chr(35) isin $left($1,1)) { var %tchan = $1- | .raw join %tchan }
  elseif ($chr(38) isin $left($1,1)) { var %tchan = $1- | .raw join %tchan }
  else { var %tchan = $chr(35) $+ $1- | .raw join %tchan  }
}
j {
  if ($1 == $null) { $ae $space(2) 4[Error]:14 not enough parameters  | serror | halt }
  if ($chr(35) isin $left($1,1)) { set %tchan $1- | .raw join %tchan }
  elseif ($chr(38) isin $left($1,1)) { var %tchan = $1- | .raw join %tchan }
  else { var %tchan = $chr(35) $+ $1- | .raw join %tchan  }
}
dos run command.com 
nick {
  if ($active == Status Window) { echo -s $space(2) • $+ $colour(nick) $+ • nick change » ( $+ $colour(nick) $$1 ) | nick $1 }
  elseif ($active == $query($active)) { echo $query($active) $space(2) • $+ $colour(nick) $+ • nick change » ( $+ $colour(nick) $$1 ) | nick $1 }
  elseif ($active != Status Window) { nick $$1 }
}
cycle {
  hop -c $active
}
p part #$1
o .onotice $1-
onotice .onotice $1- | echo #  $+ $colour(notice) $+ [ops/ $+ # $+ ] $1-
aquit if ($server != $null) { scid -a quit $1- }
h mode # +h $$1
dh mode # -h $$1
op mode # +o $$1
deop mode # -o $$1
kickr2 { var %t = $snick(#,0) | var %t2 = 1 | while (%t2 <= %t) { kick # $snick(#,%t2) $1- | inc %t2 1 } }
bankickr { var %t = $snick(#,0) | var %t2 = 1 | while (%t2 <= %t) { $odeop(#,$snick(#,%t2)) | ban # $snick(#,%t2) 3 | kick # $snick(#,%t2) | inc %t2 1 } }
banr { var %t = $snick(#,0) | var %t2 = 1 | while (%t2 <= %t) { $odeop(#,$snick(#,%t2)) | ban # $snick(#,%t2) 3 | inc %t2 1 } }
deopnick mode # -o $$1   
v { var %t = $snick(#,0) | var %t2 = 1 | while (%t2 <= %t) { mode # +v $snick(#,%t2) | inc %t2 1 } }
dv { var %t = $snick(#,0) | var %t2 = 1 | while (%t2 <= %t) { mode # -v $snick(#,%t2) | inc %t2 1 } }
voice mode # +v $$1
ping ctcp $$1 ping
whois { if ($server != $null) { %s.whois = $ctime | whois $$1 $$1 } | elseif ($server == $null) { $ae $space(2) $er not connected to server. } }
n {
  if ($1 == $null) { $ae $space(2) $er you did not specify a nick | beep | return }
  if ($2 == $null) { $ae $space(2) $er can not send notice, notice message is empty | beep | return }
  notice $1 $2- 
}
mv {
  var %w = $nick($chan,0,v)
  :loop
  if (%w >= 4 ) { set %action +vvvv | set %nickr1 $nick($chan,%w,r) | dec %w 1 | set %nickr2 $nick($chan,%w,r) | dec %w 1 | set %nickr3 $nick($chan,%w,r) | dec %w 1 | set %nickr4 $nick($chan,%w,r) | dec %w 1 | mode #  %action %nickr1 %nickr2 %nickr3 %nickr4 | goto loop }
  if (%w >= 3 ) { set %action +vvv | set %nickr1 $nick($chan,%w,r) | dec %w 1 | set %nickr2 $nick($chan,%w,r) | dec %w 1 | set %nickr3 $nick($chan,%w,r) | dec %w 1 | mode #  %action %nickr1 %nickr2 %nickr3 | goto loop }
  if (%w >= 2 ) { set %action +vv  | set %nickr1 $nick($chan,%w,r) | dec %w 1 | set %nickr2 $nick($chan,%w,r) | dec %w 1 | mode # %action %nickr1 %nickr2 | goto loop }
  if (%w >= 1 ) { set %action +v   | set %nickr1 $nick($chan,%w,r) | mode # %action %nickr1 }
  echo  # 12• mass voice 12•
}
mdv {
  var %w = $nick($chan,0,v)
  :loop
  if (%w >= 4 ) { set %action -vvvv | set %nickr1 $nick($chan,%w,v) | dec %w 1 | set %nickr2 $nick($chan,%w,v) | dec %w 1 | set %nickr3 $nick($chan,%w,v) | dec %w 1 | set %nickr4 $nick($chan,%w,v) | dec %w 1 | mode #  %action %nickr1 %nickr2 %nickr3 %nickr4 | goto loop }
  if (%w >= 3 ) { set %action -vvv | set %nickr1 $nick($chan,%w,v) | dec %w 1 | set %nickr2 $nick($chan,%w,v) | dec %w 1 | set %nickr3 $nick($chan,%w,v) | dec %w 1 | mode # %action %nickr1 %nickr2 %nickr3 | goto loop }
  if (%w >= 2 ) { set %action -vv  | set %nickr1 $nick($chan,%w,v) | dec %w 1 | set %nickr2 $nick($chan,%w,v) | dec %w 1 | mode # %action %nickr1 %nickr2 | goto loop }
  if (%w >= 1 ) { set %action -v   | set %nickr1 $nick($chan,%w,v) | mode # %action %nickr1 }
  echo # 14• mass devoice 14•
}
ignoreall {
  if  (%f12 == 1) { .ignore  -r *!*@* | .timer399 off | $ae •4• universal ignore »4 disabled | %f12 = 2 | soff | return }
  if  (%f12 == 2) { .ignore *!*@* | set %f12 1 | $ae •12• universal ignore »12 enabled | .timer399 0 50 $ae 4 universal ignore on  Type /ignoreall to disable | signoreon | return }
  elseif (%F12 == $null) { %F12 = 2 | return }
}
version {
  if ($1 != $null) { ctcp $1 version }
  else version
}
q query $1
kb if ($me isop $chan) { ban $chan $1 3 | kick $chan $1- }
k if ($me isop $chan) { kick $chan $1- }
nexgen return $true
think say 12.•º( $$1- 12)
sqoff _vw querycon switch off | echo -a [SECURE QUERY] OFF
sqon _vw querycon switch on | echo -a [SECURE QUERY] ON
fkb if ($me isop $chan) { ban $chan $snicks 3 | kick $chan $snicks }
fk if ($me isop $chan) { kick $chan $snicks }
aupdate dupdate
; Displays network configuration
; Usage: /ipconfig
ipconfig {
  run -n cmd /c ipconfig /all > $mircdiripconfig.txt && start notepad $mircdiripconfig.txt
}

; Displays current network connections
; Usage: /netstat
netstat {
  run -n cmd /c netstat -an > $mircdirnetstat.txt && start notepad $mircdirnetstat.txt
}

; Looks up dns records
; Usage /ddns mx team-nexgen.com
; $1 = a, any, cname, mx, ns, ptr, soa, srv
; $2 = domain name or ip
ddns {
  run -n cmd /c nslookup -type= $+ $$1 $$2 > $mircdirddns.txt && start notepad $mircdirddns.txt
}

; Flushes your local dns cache
; Usage: /flushdns
flushdns { 
  run -n cmd /c ipconfig /flushdns
}

; Shows network statistics
; Usage: /net-stat workstation
; $1 = Server or Workstation
net-stat {
  run -n cmd /c net statistics $1 > $mircdirnet-stat.txt && start notepad $mircdirnet-stat.txt
}

; Starts or stops the Messenger (spam) service
; Can only start/stop the messenger service if it's not disabled
; Usage /messenger stop
; $1 = start, stop
messenger {
  if ($1 == start) {
    run -n cmd /c net start messenger
  }
  elseif ($1 == stop) {
    run -n cmd /c net stop messenger
  }
}

; Displays folder contents
; Recurses into sub-folders by default but can be changed
; Usage /listdir
listdir {
  run -n cmd /c dir /a-d /b /s $$sdir(c:\) > $mircdirdir.txt && start notepad $mircdirdir.txt
}
-l e.part
